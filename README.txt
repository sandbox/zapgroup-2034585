
-- SUMMARY --

The Zapper Scan-to-Login module displays a QR code on the log in form making it
easier for users of your site (and administrators) to register and log in. 

After a user has downloaded the Zapper application for their mobile phone, they 
can scan the QR code to register on the site, inputting their details directly 
into Zapper. Once clicking submit, the users data will be pushed to your site as
a registration request and, once successful, will log them in to their my profile
dashboard.

Any returning users can just scan the QR code again to log in to your website with
no need to remember their credentials.

For a full description of the module, visit the project page:
  http://drupal.org/project/<project_name_here>

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/<project_name_here>


-- REQUIREMENTS --

* jQuery 1.7.2 or later JavaScript library (get's included when the module is 
  enabled)


-- INSTALLATION --

* Install as usual, see https://drupal.org/documentation/install/modules-themes/modules-7 
  for further information.

* The configuration options will be under the "User Interface" section under the name 
  "Zapper Scan-to-Login"


-- CONFIGURATION --

* Configure settings in Administration » User Interface » Zapper Scan-to-Login:

  - Merchant ID

    The Merchant ID configured after you sign-up at http://zapper.com/register-s2l.php

  - Site ID

    The Site ID configured after you sign-up at http://zapper.com/register-s2l.php

  - Allow User Registration

    If this option is checked, it will allow users to register and log in to 
    your site. If you uncheck this, CURRENT users of your site will only be able
    to use the QR code to log in.


-- TROUBLESHOOTING --

* If the QR code does not show on the login form, check the following:

  - Is the module enabled? Make sure the checkbox is ticked next to the module name.
  

-- FAQ --

Q: The module is enabled but the QR code is not showing on the log in form. Why?

A: The module is written to accomodate the default drupal log in module and mark-up.
   If this is changed or altered in anyway, the QR code won't be injected into 
   the form, and thus the QR code won't be shown. Make sure that the log in form
   html attribute "id" is set to either "user-login-form" or "user-login".


Q: I'm using a custom theme, the module is enabled but the QR code isn't showing
   on the log in form. Why?

A: Our plug in form will work with most themes provided that they have stuck with 
   drupal convention and haven't re-named most of the mark-up elements. If the log in
   form html attribute "id" remains "user-login-form" or "user-login" the QR code
   will show.


Q: I'm having difficulty with top navigation bar login because the QR code is 
   too small or it does not show at all. What types of log in "styles" does the 
   module work with?

A: Currently, the module works best with side-bar log in styles or full-page log in
   styles, but will still work with top navigation or bottom navigation log in forms
   provided that the log in area is big enough to display the QR code. Side-bar and
   full-page styles are recommended though.


Q: Sometimes after scanning the QR code to log in, it takes a while to actually log 
   in and redirect the user. Why?

A: Unfortunately with any third-party application, communication speed is 
   dictated by the users connection speed and latency. The faster the connection, the
   faster the log in authentication round trip will be.