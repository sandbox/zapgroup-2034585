(function($){

  $(function() {

    var merchantId = Drupal.settings.scan_to_login.merchantId;
    var siteId = Drupal.settings.scan_to_login.siteId;
    var allowUserRegistration = Drupal.settings.scan_to_login.allowUserRegistration;
    var apiUrl = Drupal.settings.scan_to_login.apiUrl;
    var baseUrl = Drupal.settings.scan_to_login.baseUrl; // 'http://drupal-tfs.local/';
    var placeHolder = $('<div class="scantologin-qrcode-placeholder"></div>');

    if (merchantId && siteId) {

      if ($('#user-login-form').length > 0) {
        $('#user-login-form').prepend(placeHolder); 
      }
      
      if ($('#user-login').length > 0) {
        $('#user-login').prepend(placeHolder);    
      }

      if ($('#user-register-form').length > 0) {
        $('#user-register-form').prepend(placeHolder);    
      }

      var qrCode = new ZapperTech.QrCode({
        merchantId: merchantId,
        siteId: siteId,
        selector: placeHolder,
        basePollUrl: apiUrl + "clientsession",
        baseQuestionsUrl: apiUrl + "merchants/{merchantId}/sites/{siteId}/questions/{taskId}/embed?callback=?"
      });

      qrCode.registrationRequest(function(data){
        if(!allowUserRegistration) {
          var error = 'This system does not allow self registration.';
          alert(error);
          qrCode.registrationRespond({
            success: false,
            errors: [error],
            username: '',
            password: ''
          });
        } else {
          var email = data.getAnswer(qrCode.QUESTIONTYPE.email);
          var firstName = data.getAnswer(qrCode.QUESTIONTYPE.firstName);
          var lastName = data.getAnswer(qrCode.QUESTIONTYPE.lastName);
          var password = data.Password;
          $.ajax({
            url: baseUrl + '?zappertech=register',
            type: 'POST',
            data: {
              firstName: firstName,
              lastName: lastName,
              email: email,
              password: password
            },
            dataType: 'json'
          }).done(function(result){
            var response = {
              success: result.success,
              errors: result.errors,
              username: result.username,
              password: result.password
            };
            qrCode.registrationRespond(response);
            if(result.success === false) { 
              var alertMessage = "Registration failed: \n";
              $(response.errors).each(function(i, error) {
                alertMessage += error + "\n";
              });
              alert(alertMessage);
            } else {
              login({
                Username: result.username,
                Password: result.password
              });
            }
          });
        }
      });

      var login = function(data){
        $.ajax({
          url: baseUrl + '?zappertech=authenticate',
          type: 'POST',
          data: {
            username: data.Username,
            password: data.Password
          },
          dataType: 'json',
          complete: function(result){
            result = eval('(' + result.responseText + ')');
            var response = {
              success: result.success,
              errors: result.errors,
              username: result.username,
              password: result.password
            };
            qrCode.loginRespond(response);
            window.setTimeout(function() {

              if ($('#user-register-form').length > 0) {
                location.href = baseUrl + '?q=user';
              } else {
                
                if ($('form#user-login-form').length > 0) {
                  $form = $('form#user-login-form').clone();  
                } else {
                  $form = $('form#user-login').clone();
                }

                $('#edit-name', $form).val(response.username);
                $('#edit-pass', $form).val(response.password);
                $form.submit();
              }
            }, 1000)
          }
        });
      };
      
      qrCode.loginRequest(login);
      qrCode.start()
    }
  });

})(jQuery);